# Task Manager - authentication + CRUD

### Front-end: Vue 3, Vuex
### Back-end: Node.js Express
### Database: MongoDB

### Demo: https://vue-task-manager-client.onrender.com

## Server project setup
```
cd server
```

```
npm install
```

```
npm run dev
```

## Client project setup
```
cd client
```

```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

## INFO:
### Sendgrid 
- Settings/Sender Authentication/Create a Sender
env FROM_EMAIL the same as in sendgrid dashboard
- Settings/API Keys/Create api key
- SENDGRID_USERNAME = apikey

### Render node app
- add environment variables and add node files

### Mongo Atlas 
- MongoDB Atlas -> NetworkAccess -> Edit -> Allowed Access from Anywhere
- 0.0.0.0/0 allows access from anywhere

### Render Vue client app
- add environment variables 