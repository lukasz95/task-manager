const nodemailer = require('nodemailer');

const sendEmail = async (options) => {
  // Sendgrid transporter
  const transporter = nodemailer.createTransport({
      service: 'SendGrid',
      auth: {
        user: process.env.SENDGRID_USERNAME,
        pass: process.env.SENDGRID_PASSWORD
      }
  })

  // Define email options
  const transporterOptions = {
    from: `${process.env.FROM_NAME} <${process.env.FROM_EMAIL}>`,
    to: options.email,
    subject: options.subject,
    text: options.message,
  };

  // Send the actual email
  await transporter.sendMail(transporterOptions);
};

module.exports = sendEmail;