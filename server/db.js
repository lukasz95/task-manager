const mongoose = require('mongoose');

module.exports = () => {
  const DB = process.env.DB;
  mongoose
    .connect(DB, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    .then(() => console.log('Connected successfully to DB'))
    .catch(() => console.error('Could not connect to DB'));
};
