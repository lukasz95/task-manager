const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: [true, 'Task name is required'],
      maxlength: [250, 'Task name must not exceed 250 characters']
    },
    description: {
      type: String,
      trim: true,
      required: [true, 'Task description is required'],
      maxlength: [3000, 'Task description must not exceed 3000 characters']
    }
  },
  {
    timestamps: true,
  }
);

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;