const Task = require('../models/taskModel');

// @desc      Create task
// @route     POST /api/task
// @access    Public
exports.createTask = async (req, res, next) => {
  try {
    // create a new task
    const newTask = await Task.create({
      name: req.body.name,
      description: req.body.description
    });

    // response
    return res.status(200).json({
      status: 200,
      success: true,
      message: 'Task created',
      task: newTask
    });
  } catch (err) {
    next(err);
  }
};

// @desc      Get task list
// @route     GET /api/tasks
// @access    Public
exports.getTaskList = async (req, res, next) => {
  try {
    // get tasks
    const tasks = await Task.find()

    // response
    return res.status(200).json({
      status: 200,
      success: true,
      message: 'Tasks have been found',
      tasks: tasks
    });
  } catch (err) {
    next(err);
  }
};

// @desc      Delete task
// @route     DELETE /api/task/:id
// @access    Public
exports.deleteTask = async (req, res, next) => {
  try {
    // delete task
    const task = await Task.findById(req.params.id);

    if (!task) {
      return res.status(404).json({
        status: 404,
        success: false,
        error: 'Task not found'
      });
    }

    await task.remove();

    // response
    return res.status(200).json({
      status: 200,
      success: true,
      message: 'Task has been deleted'
    });
  } catch (err) {
    next(err);
  }
};

// @desc      Update task
// @route     PUT /api/task/:id
// @access    Public
exports.updateTask = async (req, res, next) => {
  try {
    // update task
    let task = await Task.findById(req.params.id);

    if (!task) {
      return res.status(404).json({
        status: 404,
        success: false,
        error: 'Task not found'
      });
    }

    task = await Task.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });
  
    task.save();

    // response
    return res.status(200).json({
      status: 200,
      success: true,
      message: 'Task has been updated',
      task: task
    });
  } catch (err) {
    next(err);
  }
};

// @desc      Get single task
// @route     GET /api/task/:id
// @access    Public
exports.getTask = async (req, res, next) => {
  try {
    // get task
    let task = await Task.findById(req.params.id);

    if (!task) {
      return res.status(404).json({
        status: 404,
        success: false,
        error: 'Task not found'
      });
    }

    // response
    return res.status(200).json({
      status: 200,
      success: true,
      message: 'Task has been found',
      task: task
    });
  } catch (err) {
    next(err);
  }
};