const User = require('../models/userModel');
const generateToken = require('../utilities/generateToken');
const sendEmail = require('../utilities/sendEmail');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

// @desc      Get user
// @route     GET /api/auth/getUser
// @access    Public
exports.getUser = async (req, res, next) => {

  const token = req.cookies.token;

  if (!token) {
    return res.status(403).json({
      status: 403,
      success: false,
      error: 'Not authorized access'
    });
  }
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    const user = (await User.findById(decoded.id)) || null;

    if (user) {
      return res.status(200).json({
        status: 200,
        success: true,
        message: 'User exists',
        user: user
      });
    } else {
      return res.status(404).json({
        status: 404,
        success: false,
        error: 'User not found'
      });
    }
  } catch (err) {
    next(err);
  }
};

// @desc      Register user
// @route     POST /api/auth/register
// @access    Public
exports.register = async (req, res, next) => {
  try {
    // create a new user
    const newUser = await User.create({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    });

    // cookie options
    const options = {
      expires: new Date(
        Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
      ),
      httpOnly: true,
      secure: true,
      sameSite: 'none'
    };
    
    // response
    return res.status(201).cookie('token', generateToken(newUser._id), options).json({
      status: 201,
      success: true,
      message: 'Created a new user',
      user: {
        _id: newUser._id,
        name: newUser.name,
        email: newUser.email,
        createdAt: newUser.createdAt
      }
    });
  } catch (err) {
    next(err);
  }
};

// @desc      Log in user
// @route     POST /api/auth/logIn
// @access    Public
exports.logIn = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    // validate email & password
    if (!email || !password) {
      return res.status(400).json({
        status: 400,
        success: false,
        error: 'Please provide an email and password'
      });
    }

    // check for user
    const user = await User.findOne({ email }).select('+password');

    if (!user) {
      return res.status(401).json({
        status: 401,
        success: false,
        error: 'Invalid credentials'
      });
    }

    // check if password matches
    const isMatch = await user.matchPassword(password);

    if (!isMatch) {
      return res.status(401).json({
        status: 401,
        success: false,
        error: 'Invalid credentials'
      });
    }

    if (user) {
      // cookie options
      const options = {
        expires: new Date(
          Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
        ),
        httpOnly: true,
        secure: true,
        sameSite: 'none'
      };

      // response
      return res.status(200).cookie('token', generateToken(user._id), options).json({
        status: 200,
        success: true,
        message: 'Logged in successfully',
        user: {
          _id: user._id,
          name: user.name,
          email: user.email,
          createdAt: user.createdAt
        }
      });
    }
  } catch (err) {
    next(err);
  }
};

// @desc      Log out
// @route     POST /api/auth/logOut
// @access    Public
exports.logOut = async (req, res, next) => {
  const token = req.cookies.token;

  if (!token) {
    return res.status(403).json({
      status: 403,
      success: false,
      error: 'Not authorized access'
    });
  }

  // cookie options
  const options = {
    expires: new Date(
      Date.now() - 1 * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
    secure: true,
    sameSite: 'none'
  };
  
  try {
    res.status(200).cookie('token', 'none', options).json({
      status: 200,
      success: true,
      message: 'Logged out successfully'
    });
  }
  catch (err) {
    next(err);
  }
}

// @desc      Reset password
// @route     POST /api/auth/resetPassword
// @access    Public
exports.resetPassword = async (req, res, next) => {
  const email = req.body.email;

  // validate email
  if (!email) {
    return res.status(400).json({
      status: 400,
      success: false,
      error: 'Please provide an email address'
    });
  }

  // check for user
  const user = await User.findOne({ email: email });

  if (!user) {
    return res.status(401).json({
      status: 401,
      success: false,
      error: 'Invalid credentials'
    });
  }

  // Get reset token
  const resetToken = user.getResetPasswordToken();

  await user.save({ validateBeforeSave: false });

  // Create reset url
  const resetUrl = `https://vue-task-manager-client.onrender.com/update-password/${resetToken}`;

  const message = `Hello,\n\nFollow this link to reset your Task Manager password for your account.\n\n${resetUrl}\n\nIf you didn't ask to reset your password, you can ignore this email.\n\nThanks,\n\nTask Manager`;

  try {
    await sendEmail({
      email: user.email,
      subject: 'Reset your password for Task Manager',
      message,
    });

    return res.status(200).json({
      status: 200,
      success: true,
      message: `Email sent to ${user.email}`,
    });
  } catch (err) {
    console.log(err);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    await user.save({
      validateBeforeSave: false,
    });
    
    return res.status(500).json({
      status: 500,
      success: false,
      error: 'Email could not be send',
    });
  }
};

// @desc      Update password
// @route     POST /api/auth/updatePassword
// @access    Public
exports.updatePassword = async (req, res, next) => {

  const { password, confirmPassword, token } = req.body;

  // validate email, password & token
  if (!password || !confirmPassword || !token) {
    return res.status(400).json({
      status: 400,
      success: false,
      error: 'Please provide all required fields'
    });
  }

  // check passwords
  if (password !== confirmPassword) {
    return res.status(400).json({
      status: 400,
      success: false,
      error: 'Given passwords do not match'
    });
  }

  // 1) Get user based on the token
  const hashedToken = crypto
    .createHash('sha256')
    .update(token)
    .digest('hex');

  const user = await User.findOne({
    resetPasswordToken: hashedToken,
    resetPasswordExpire: { $gt: Date.now() }
  });

  // 2) If token has not expired, and there is user, set the new password
  if (!user) {
    return res.status(400).json({
      status: 400,
      success: false,
      error: 'Token is invalid or has expired'
    });
  }

  // Set new password
  user.password = password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpire = undefined;
  await user.save();

  return res.status(200).json({
    status: 200,
    success: true,
    message: 'Password successfully updated',
  });
};
