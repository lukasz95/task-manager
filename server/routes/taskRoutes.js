const express = require('express');
const taskController = require('../controllers/taskController');

const router = express.Router();

router.post('/task', taskController.createTask);
router.get('/tasks', taskController.getTaskList);
router.delete('/task/:id', taskController.deleteTask);
router.put('/task/:id', taskController.updateTask);
router.get('/task/:id', taskController.getTask);

module.exports = router;