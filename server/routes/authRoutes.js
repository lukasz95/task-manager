const express = require('express');
const authController = require('../controllers/authController');

const router = express.Router();

router.post('/register', authController.register);
router.get('/getUser', authController.getUser);
router.post('/logOut', authController.logOut);
router.post('/logIn', authController.logIn);
router.post('/resetPassword', authController.resetPassword);
router.post('/updatePassword', authController.updatePassword);

module.exports = router;
