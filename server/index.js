const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const authRouter = require('./routes/authRoutes');
const taskRoutes = require('./routes/taskRoutes');
const connectDB = require('./db');
const errorHandler = require('./middleware/errorHandler');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require("helmet");
const xss = require('xss-clean')
const hpp = require('hpp');
const rateLimit = require("express-rate-limit");
const compression = require('compression');

// Uncaught Exceptions
process.on('uncaughtException', (err) => {
  console.log('Uncaught Exception');
  console.log(err);
  process.exit(1);
});

dotenv.config();

// Connect do database
connectDB();

const app = express();
app.use(express.json());
// CORS
if (process.env.NODE_ENV === 'production') {
  const whitelist = ['https://vue-task-manager-client.onrender.com']
  corsOptions = {
    origin: function (origin, callback) {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    },
    credentials: true,
    optionsSuccessStatus: 200,
    exposedHeaders: ['set-cookie']
  }
} else {
  corsOptions = {
    origin: true,
    credentials: true,
    optionsSuccessStatus: 200,
    exposedHeaders: ['set-cookie']
  }
}
app.use(cors(corsOptions));


app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));
app.use(cookieParser())

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(mongoSanitize());
app.use(helmet());
app.use(xss());
app.use(hpp());
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 200 // limit each IP to 100 requests per windowMs
});
app.use(limiter);

app.use(compression())

// Routes
app.use('/api/auth', authRouter);
app.use('/api', taskRoutes);

app.use(errorHandler);

// Handling Unhandled Routes
app.all('*', (req, res, next) => {
  res.status(404).json({
    status: 404,
    message: 'Not Found',
  });
});

// start server
const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => {
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`);
});
// const server = app.listen();

// Unhandled Rejections
process.on('unhandledRejection', (err) => {
  console.log('Unhandled Rejection');
  console.log(err);
  server.close(() => process.exit(1));
});


process.on('SIGTERM', () => {
  console.log('SIGTERM RECEIVED. Shutting down application');
  server.close(() => {
    console.log('Process terminated!');
  });
});