import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/HomePage/HomePage.vue'
import store from '../store/index'

const ifNotAuthenticated = (to, from, next) => {
  const user = store.state.user.user
  if (user) {
    next()
  } else {
    next({ name: 'LoginPage' })
  }
}

const ifAuthenticated = (to, from, next) => {
  const user = store.state.user.user
  if (user) {
    next({ name: 'ProfilePage' })
  } else {
    next()
  }
}

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage
  },
  {
    path: '/404',
    name: '_404Page',
    component: () => import(/* webpackChunkName: "_404Page" */ '../views/_404Page/_404Page.vue')
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/404'
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: () => import(/* webpackChunkName: "login" */ '../views/LoginPage/LoginPage.vue'),
    beforeEnter: ifAuthenticated /* => ProfilePage */
  },
  {
    path: '/register',
    name: 'RegisterPage',
    component: () => import(/* webpackChunkName: "register" */ '../views/RegisterPage/RegisterPage.vue'),
    beforeEnter: ifAuthenticated /* => ProfilePage */
  },
  {
    path: '/reset-password',
    name: 'ResetPasswordPage',
    component: () => import(/* webpackChunkName: "resetPassword" */ '../views/ResetPasswordPage/ResetPasswordPage.vue'),
    beforeEnter: ifAuthenticated /* => ProfilePage */
  },
  {
    path: '/update-password/:id',
    name: 'UpdatePasswordPage',
    component: () => import(/* webpackChunkName: "updatePassword" */ '../views/UpdatePasswordPage/UpdatePasswordPage.vue'),
    beforeEnter: ifAuthenticated /* => ProfilePage */
  },
  {
    path: '/profile',
    name: 'ProfilePage',
    component: () => import(/* webpackChunkName: "profile" */ '../views/ProfilePage/ProfilePage.vue'),
    beforeEnter: ifNotAuthenticated /* => LoginPage */
  },
  {
    path: '/edit-task/:id',
    name: 'EditTaskPage',
    component: () => import(/* webpackChunkName: "editTask" */ '../views/EditTaskPage/EditTaskPage.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  store.dispatch('getUser').then(() => {
    next()
  })
})

export default router
