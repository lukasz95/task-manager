import { createStore } from 'vuex'
import user from './modules/user'
import task from './modules/task'

export default createStore({
  modules: {
    user,
    task
  }
})
