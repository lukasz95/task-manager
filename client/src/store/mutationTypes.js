// Global
export const ERROR = 'ERROR'
export const PENDING = 'PENDING'
export const DB_CHECK = 'DB_CHECK'
export const FEEDBACK = 'FEEDBACK'

// User
export const SET_USER = 'SET_USER'
export const USER_PENDING = 'USER_PENDING'

// Task
export const TASK_FORM_ERROR = 'TASK_FORM_ERROR'
export const TASK_FORM_PENDING = 'TASK_FORM_PENDING'
export const TASK_FORM_FEEDBACK = 'TASK_FORM_FEEDBACK'
export const SET_TASK_LIST = 'SET_TASK_LIST'
export const TASK_LIST_ERROR = 'TASK_LIST_ERROR'
export const TASK_LIST_PENDING = 'TASK_LIST_PENDING'
export const ADD_TASK = 'ADD_TASK'
export const TASK_LIST_FEEDBACK = 'TASK_LIST_FEEDBACK'
export const REMOVE_TASK = 'REMOVE_TASK'
export const TASK_PENDING = 'TASK_PENDING'
export const SET_TASK = 'SET_TASK'
export const SET_TASK_FROM_STATE = 'SET_TASK_FROM_STATE'
export const UPDATE_TASK = 'UPDATE_TASK'
