import axios from 'axios'
import router from '../../router/index'

import { ERROR, PENDING, SET_USER, DB_CHECK, USER_PENDING, FEEDBACK } from '../mutationTypes'

const LOG_IN_ENDPOINT = process.env.VUE_APP_LOG_IN_ENDPOINT
const GET_USER_ENDPOINT = process.env.VUE_APP_GET_USER_ENDPOINT
const LOG_OUT_ENDPOINT = process.env.VUE_APP_LOG_OUT_ENDPOINT
const REGISTER_ENDPOINT = process.env.VUE_APP_REGISTER_ENDPOINT
const RESET_PASSWORD_ENDPOINT = process.env.VUE_APP_RESET_PASSWORD_ENDPOINT
const UPDATE_PASSWORD_ENDPOINT = process.env.VUE_APP_UPDATE_PASSWORD_ENDPOINT

const user = {
  state: {
    error: null,
    user: null,
    pending: false,
    userPending: false,
    dbCheck: false,
    feedback: false
  },
  getters: {
    getError (state) {
      return state.error
    },
    getPending (state) {
      return state.pending
    },
    getUserPending (state) {
      return state.userPending
    },
    getUser (state) {
      return state.user
    },
    getFeedback (state) {
      return state.feedback
    }
  },
  mutations: {
    [ERROR] (state, value) {
      state.error = value
    },
    [PENDING] (state, value) {
      state.pending = value
    },
    [USER_PENDING] (state, value) {
      state.userPending = value
    },
    [SET_USER] (state, value) {
      state.user = value
    },
    [DB_CHECK] (state, value) {
      state.dbCheck = value
    },
    [FEEDBACK] (state, value) {
      state.feedback = value
    }
  },
  actions: {
    clearError (context) {
      context.commit(ERROR, null)
    },
    // setPending (context, payload) {
    //   context.commit(PENDING, payload.pending)
    // },
    clearFeedback (context, payload) {
      context.commit(FEEDBACK, false)
    },
    async getUser (context) {
      context.commit(USER_PENDING, true)
      if (!context.state.user && !context.state.dbCheck) {
        await axios({
          method: 'get',
          url: GET_USER_ENDPOINT,
          headers: {
            'Content-Type': 'application/json'
          },
          withCredentials: true
        })
          .then(response => {
            if (response.data.success === true) {
              context.commit(SET_USER, response.data.user)
            }
            context.commit(USER_PENDING, false)
            context.commit(DB_CHECK, true)
          })
          .catch(error => {
            context.commit(ERROR, error.response.data.error.toString().replace(/,/g, ', '))
            context.commit(USER_PENDING, false)
            context.commit(DB_CHECK, true)
          })
      } else {
        context.commit(USER_PENDING, false)
        context.commit(DB_CHECK, true)
      }
    },
    async register (context, payload) {
      context.commit(PENDING, true)
      await axios({
        method: 'post',
        url: REGISTER_ENDPOINT,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          name: payload.name,
          email: payload.email,
          password: payload.password
        },
        withCredentials: true
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(SET_USER, response.data.user)
            context.commit(PENDING, false)
            router.push({ name: 'ProfilePage' })
          } else {
            context.commit(ERROR, 'An error occurred, please try again later')
            context.commit(PENDING, false)
          }
        })
        .catch((error) => {
          context.commit(ERROR, error.response.data.error.toString().replace(/,/g, ', '))
          context.commit(PENDING, false)
        })
    },
    async logIn (context, payload) {
      context.commit(PENDING, true)
      await axios({
        method: 'post',
        url: LOG_IN_ENDPOINT,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          email: payload.email,
          password: payload.password
        },
        withCredentials: true
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(SET_USER, response.data.user)
            context.commit(PENDING, false)
            router.push({ name: 'ProfilePage' })
          } else {
            context.commit(ERROR, 'An error occurred, please try again later')
            context.commit(PENDING, false)
          }
        })
        .catch((error) => {
          context.commit(ERROR, error.response.data.error.toString().replace(/,/g, ', '))
          context.commit(PENDING, false)
        })
    },
    async logOut (context) {
      await axios({
        method: 'post',
        url: LOG_OUT_ENDPOINT,
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true
      })
        .then(response => {
          if (response.data.success === true) {
            router.push({ name: 'HomePage' }).catch(() => {})
            context.commit(SET_USER, null)
          } else {
            context.commit(ERROR, 'An error occurred, please try again later')
          }
        })
        .catch((error) => {
          context.commit(ERROR, error.response.data.error.toString().replace(/,/g, ', '))
        })
    },
    async resetPassword (context, payload) {
      context.commit(PENDING, true)
      await axios({
        method: 'post',
        url: RESET_PASSWORD_ENDPOINT,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          email: payload.email
        }
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(FEEDBACK, true)
            context.commit(PENDING, false)
          } else {
            context.commit(ERROR, 'An error occurred, please try again later')
            context.commit(PENDING, false)
          }
        })
        .catch((error) => {
          context.commit(ERROR, error.response.data.error.toString().replace(/,/g, ', '))
          context.commit(PENDING, false)
        })
    },
    async updatePassword (context, payload) {
      context.commit(PENDING, true)
      await axios({
        method: 'post',
        url: UPDATE_PASSWORD_ENDPOINT,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          password: payload.password,
          confirmPassword: payload.confirmPassword,
          token: payload.token
        }
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(FEEDBACK, true)
            context.commit(PENDING, false)
          } else {
            context.commit(ERROR, 'An error occurred, please try again later')
            context.commit(PENDING, false)
          }
        })
        .catch((error) => {
          context.commit(ERROR, error.response.data.error.toString().replace(/,/g, ', '))
          context.commit(PENDING, false)
        })
    }
  }
}

export default user
