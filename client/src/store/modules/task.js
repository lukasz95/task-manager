import axios from 'axios'
import router from '../../router/index'

import { TASK_FORM_ERROR, TASK_FORM_PENDING, TASK_FORM_FEEDBACK, SET_TASK_LIST, TASK_LIST_ERROR, TASK_LIST_PENDING, ADD_TASK, TASK_LIST_FEEDBACK, REMOVE_TASK, TASK_PENDING, SET_TASK, SET_TASK_FROM_STATE, UPDATE_TASK } from '../mutationTypes'

const CREATE_TASK_ENDPOINT = process.env.VUE_APP_CREATE_TASK_ENDPOINT
const GET_TASK_LIST_ENDPOINT = process.env.VUE_APP_GET_TASK_LIST_ENDPOINT
const GET_TASK_ENDPOINT = process.env.VUE_APP_GET_TASK_ENDPOINT
const UPDATE_TASK_ENDPOINT = process.env.VUE_APP_UPDATE_TASK_ENDPOINT

const task = {
  state: {
    taskFormError: null,
    taskFormPending: false,
    taskFormFeedback: false,
    taskList: null,
    taskListError: null,
    taskListPending: false,
    taskListFeedback: false,
    task: null,
    taskPending: false,
    taskError: null
  },
  getters: {
    getTaskFormError (state) {
      return state.taskFormError
    },
    getTaskFormPending (state) {
      return state.taskFormPending
    },
    getTaskFormFeedback (state) {
      return state.taskFormFeedback
    },
    getTaskList (state) {
      return state.taskList
    },
    getTaskListError (state) {
      return state.taskListError
    },
    getTaskListPending (state) {
      return state.taskListPending
    },
    getTaskListFeedback (state) {
      return state.taskListFeedback
    },
    getTask (state) {
      return state.task
    },
    getTaskPending (state) {
      return state.taskPending
    },
    getTaskError (state) {
      return state.taskError
    }
  },
  mutations: {
    [TASK_FORM_ERROR] (state, value) {
      state.taskFormError = value
    },
    [TASK_FORM_PENDING] (state, value) {
      state.taskFormPending = value
    },
    [TASK_FORM_FEEDBACK] (state, value) {
      state.taskFormFeedback = value
    },
    [SET_TASK_LIST] (state, value) {
      state.taskList = value
    },
    [TASK_LIST_ERROR] (state, value) {
      state.taskListError = value
    },
    [TASK_LIST_PENDING] (state, value) {
      state.taskListPending = value
    },
    [ADD_TASK] (state, value) {
      state.taskList.push(value)
    },
    [TASK_LIST_FEEDBACK] (state, value) {
      state.taskListFeedback = value
    },
    [REMOVE_TASK] (state, value) {
      const task = state.taskList.find(item => item._id === value)
      state.taskList.splice(state.taskList.indexOf(task), 1)
    },
    [SET_TASK] (state, value) {
      state.task = value
    },
    [TASK_PENDING] (state, value) {
      state.taskPending = value
    },
    [SET_TASK_FROM_STATE] (state, value) {
      const task = state.taskList.find(item => item._id === value)
      if (!task) {
        router.push('/404')
        return
      }
      state.task = task
    },
    [UPDATE_TASK] (state, value) {
      if (state.taskList) {
        const task = state.taskList.find(item => item._id === value._id)
        task.name = value.name
        task.description = value.description
      }
    }
  },
  actions: {
    clearTaskFormError (context) {
      context.commit(TASK_FORM_ERROR, null)
    },
    clearTaskFormFeedback (context, payload) {
      context.commit(TASK_FORM_FEEDBACK, false)
    },
    async createTask (context, payload) {
      context.commit(TASK_FORM_PENDING, true)
      await axios({
        method: 'post',
        url: CREATE_TASK_ENDPOINT,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          name: payload.name,
          description: payload.description
        }
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(TASK_FORM_FEEDBACK, true)
            context.commit(TASK_FORM_PENDING, false)
            context.commit(ADD_TASK, response.data.task)
          } else {
            context.commit(TASK_FORM_PENDING, false)
            context.commit(TASK_FORM_ERROR, 'An error occurred, please try again later')
          }
        })
        .catch((error) => {
          context.commit(TASK_FORM_PENDING, false)
          context.commit(TASK_FORM_ERROR, error.response.data.error.toString().replace(/,/g, ', '))
        })
    },
    clearTaskListError (context) {
      context.commit(TASK_LIST_ERROR, null)
    },
    async getTaskList (context, payload) {
      context.commit(TASK_LIST_PENDING, true)
      if (!context.state.taskList) {
        await axios({
          method: 'get',
          url: GET_TASK_LIST_ENDPOINT,
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(response => {
            if (response.data.success === true) {
              context.commit(SET_TASK_LIST, response.data.tasks)
              context.commit(TASK_LIST_PENDING, false)
            } else {
              context.commit(TASK_LIST_ERROR, 'An error occurred, please try again later')
              context.commit(TASK_LIST_PENDING, false)
            }
          })
          .catch((error) => {
            context.commit(TASK_FORM_ERROR, error.response.data.error.toString().replace(/,/g, ', '))
            context.commit(TASK_LIST_PENDING, false)
          })
      } else {
        context.commit(TASK_LIST_PENDING, false)
      }
    },
    async deleteTask (context, payload) {
      await axios({
        method: 'delete',
        url: CREATE_TASK_ENDPOINT + '/' + payload.taskId,
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(TASK_LIST_FEEDBACK, true)
            context.commit(REMOVE_TASK, payload.taskId)
          } else {
            context.commit(TASK_LIST_ERROR, 'An error occurred, please try again later')
          }
        })
        .catch((error) => {
          context.commit(TASK_LIST_ERROR, error.response.data.error.toString().replace(/,/g, ', '))
        })
    },
    clearTaskListFeedback (context) {
      context.commit(TASK_LIST_FEEDBACK, null)
    },
    async getTask (context, payload) {
      context.commit(TASK_PENDING, true)
      if (!context.state.taskList) {
        await axios({
          method: 'get',
          url: GET_TASK_ENDPOINT + '/' + payload.taskId,
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(response => {
            if (response.data.success === true) {
              context.commit(SET_TASK, response.data.task)
              context.commit(TASK_PENDING, false)
            } else {
              context.commit(TASK_PENDING, false)
              router.push('/404')
            }
          })
          .catch(() => {
            context.commit(TASK_PENDING, false)
            router.push('/404')
          })
      } else {
        context.commit(SET_TASK_FROM_STATE, payload.taskId)
        context.commit(TASK_PENDING, false)
      }
    },
    async updateTask (context, payload) {
      context.commit(TASK_FORM_PENDING, true)
      await axios({
        method: 'put',
        url: UPDATE_TASK_ENDPOINT + '/' + payload.taskId,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          name: payload.name,
          description: payload.description
        }
      })
        .then(response => {
          if (response.data.success === true) {
            context.commit(TASK_FORM_FEEDBACK, true)
            context.commit(TASK_FORM_PENDING, false)
            context.commit(UPDATE_TASK, response.data.task)
          } else {
            context.commit(TASK_FORM_PENDING, false)
            context.commit(TASK_FORM_ERROR, 'An error occurred, please try again later')
          }
        })
        .catch((error) => {
          context.commit(TASK_FORM_PENDING, false)
          context.commit(TASK_FORM_ERROR, error.response.data.error.toString().replace(/,/g, ', '))
        })
    }
  }
}

export default task
